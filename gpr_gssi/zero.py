import numpy as np
def shift(row,cut=20):
    row = np.array(row)
    mi =    (row<=np.roll(row,1)) & (row<=np.roll(row,-1)) &\
            (row< np.roll(row,2)) & (row< np.roll(row,-2)) &\
            (row< np.roll(row,3)) & (row< np.roll(row,-3)) 
    mi = np.where(mi)[0]
    mi = mi[mi>cut][0]
    row = np.roll(row,-mi)
    row[len(row)-mi:] = np.zeros(mi)
    return row
    
