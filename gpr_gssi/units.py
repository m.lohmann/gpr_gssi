import scipy.constants as scico
import numpy as np

def depth(n_samples=1024,range_ns=20e-9,epsr=9.):
    return range_ns/2./np.sqrt(scico.mu_0*scico.epsilon_0*epsr)

def depth_arr(n_samples=1024,range_ns=20e-9,epsr=9.):
    dep = np.arange(0,
                    range_ns/2./np.sqrt(scico.mu_0*scico.epsilon_0*epsr),
                    1./n_samples)
    return dep

