from __future__ import print_function
import numpy as np
import struct, bitstruct
import pytz
from datetime import datetime, timedelta
import h5py
import sys,os

# the GSSI field unit used
UNIT = {
    0: 'unknown system type',
    1: 'unknown system type',
    2: 'SIR 2000',
    3: 'SIR 3000',
    4: 'TerraVision',
    6: 'SIR 20',
    7: 'StructureScan Mini',
    8: 'SIR 4000',
    9: 'SIR 30',
    10: 'SIR 30', # 10 is undefined in documentation but SIR 30 according to Lynn's DZX
    11: 'unknown system type',
    12: 'UtilityScan DF',
    13: 'HS',
    14: 'StructureScan Mini XT',
}

def readbit(bits, start, end):
    try:
        if start == 0:
            return bitstruct.unpack('<u'+str(end+1), bits)[0]
        else: return bitstruct.unpack('<p'+str(start)+'u'+str(end-start), bits)[0]
    except:
        print('error reading bits')

def readtime(bits):
    garbagedate = datetime(1980,1,1,0,0,0,0,tzinfo=pytz.UTC)
    if bits == '\x00\x00\x00\x00':
        # if there is no date information, return arbitrary datetime
        return garbagedate 
    else:
        try:
            # if there is date info, try to unpack
            sec2, mins, hr, day, mo, yr = bitstruct.unpack('<u5u6u5u5u4u7', bits) 
            # year+1980 should equal real year
            # sec2 * 2 should equal real seconds
            return datetime(yr+1980, mo, day, hr, mins, sec2*2, 0, tzinfo=pytz.UTC)
        except:
            # most of the time the info returned is garbage, 
            # so we return arbitrary datetime again
            return garbagedate 

def readgssi(infile, antfreq=200, stack=1,MINHEADSIZE=1024,PAREASIZE=128):
    rh_antname = ''
    with open(infile, 'rb') as f:
        # open the binary, start reading chunks
        header = {
        "filename"  : infile[:-4],
        # 0x00ff if header, 0xfnff if old file format
        "TAG"        : struct.unpack('<h', f.read(2))[0], 
        # offset to data from beginning of file
        "offsetdata"       : struct.unpack('<h', f.read(2))[0], 
        # samples per scan
        "nsamp"      : struct.unpack('<h', f.read(2))[0], 
        # bits per data word
        "bpw"       : struct.unpack('<h', f.read(2))[0], 
        # if sir-30 or utilityscan df, then repeats per sample;
        # otherwise 0x80 for 8bit and 0x8000 for 16bit
        "zero"       : struct.unpack('<h', f.read(2))[0], 
        # scans per second
        "sps"       : struct.unpack('<f', f.read(4))[0], 
        # scans per meter
        "spm"       : struct.unpack('<f', f.read(4))[0], 
        # meters per mark
        "mpm"       : struct.unpack('<f', f.read(4))[0], 
        # position (ns)
        "position"  : struct.unpack('<f', f.read(4))[0], 
        # range (ns)
        "range"     : struct.unpack('<f', f.read(4))[0], 
        # number of passes for 2-D files
        "npass"      : struct.unpack('<h', f.read(2))[0]} 

        # ensure correct read position for rfdatebyte
        f.seek(31) 

        header.update({
        # creation date and time in bits, 
        # structured as little endian u5u6u5u5u4u7
        "cdt" : readtime(f.read(4)), 
        # modification date and time in bits, 
        # structured as little endian u5u6u5u5u4u7
        "mdt" : readtime(f.read(4)) 
        })

        # skip across some proprietary stuff
        f.seek(44) 

        header.update({
        # offset to text
        "text"   : struct.unpack('<h', f.read(2))[0], 
        # size of text
        "ntext"  : struct.unpack('<h', f.read(2))[0], 
        # offset to processing history
        "proc"   : struct.unpack('<h', f.read(2))[0], 
        # size of processing history
        "nproc"  : struct.unpack('<h', f.read(2))[0], 
        # number of channels
        "nchan"  : struct.unpack('<h', f.read(2))[0], 
        # average dilectric
        "epsr"  : struct.unpack('<f', f.read(4))[0], 
        # position in meters (useless?)
        "top"   : struct.unpack('<f', f.read(4))[0], 
        # range in meters
        "depth" : struct.unpack('<f', f.read(4))[0] 
        })

        # start of antenna bit
        f.seek(98) 

        rh_ant = struct.unpack('<14c', f.read(14))
        
        # this is a blatant hack to read antenna information without putting any binary in the string
	for curr_byte in rh_ant:
            if curr_byte != b'\x00':
                rh_antname += curr_byte.decode('utf-8')

        header.update({
        "antname" : rh_antname 
        })
        
        f.seek(113) # skip to something that matters

        # byte containing versioning bits
        vsbyte = f.read(1) 
        # the system type (values in UNIT={...} dictionary above)
        rh_system = readbit(vsbyte, 3, 7) 
        del vsbyte

        header.update({
        "system" : UNIT[rh_system]
        })
        

        # whether or not the header is normal or big-->determines offset to data array
        if header["offsetdata"] < MINHEADSIZE: 
            f.seek(MINHEADSIZE * header["offsetdata"])
        else:
            f.seek(MINHEADSIZE * header["nchan"])

        if header["bpw"] == 8:
            data = np.fromfile(f, np.uint8).reshape(-1,header["nsamp"]).T
        elif header["bpw"] == 16:
            data = np.fromfile(f, np.uint16).reshape(-1,header["nsamp"]).T
        else:
            data = np.fromfile(f, np.int32).reshape(-1,header["nsamp"]).T
        data = data[2:]
        header["nsamp_cor"] = header["nsamp"]-2

    header.update({
    "seconds"   : data.shape[1]/header["sps"],
    "meters"    : data.shape[1]/header["spm"]
    })

    return header,data


if __name__ == "__main__":
    
    header, data = readgssi(sys.argv[1], antfreq=200, stack=1)
    for key in sorted(header):
        print(key, header[key])

    """
    f = h5py.File("measurement.hdf5","a")
    dset = f.create_dataset(header["filename"], data.shape,
                                                chunks=True,
                                                compression="gzip",
                                                compression_opts=9,
                                                data=data)
    for key in header:
        if key != "cdt" and key != "mdt":
            dset.attrs[key] = header[key]
    f.close()
    """

    #plotting
    import matplotlib.pyplot as plt
    f,ax=plt.subplots()
    ax.imshow(  data,
                extent=(0.,header["meters"],0.,header["range"]),
                origin="lower")
    ax.invert_yaxis()
    ax.set_xlabel("distance/m")
    ax.set_ylabel("depth/ns")
    plt.show()
