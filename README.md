# GPR GSSI reader

This is a small API for getting data out of GSSI's .DZT files from the ground penetrating radar (gpr) used at the institute for [Geophysics](http://www.uni-goettingen.de/de/538888.html>) in Göttingen.
The original script and implementation was taken from [Ian Nesbitt's Github page](https://github.com/iannesbitt/readgssi).
Some of the comments in the text might still be from him.

This API sole purpose is to extract the header and data parts of the .DZT file and save it in a HDF5 file format.
This can further be used to perform some simple tasks like zero correction and plotting.
