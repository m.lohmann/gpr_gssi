from setuptools import setup

setup(name='gpr_gssi',
      version='0.1',
      description='Reader for GPR data in gssi file (.dzt) format',
      url='https://gitlab.gwdg.de/kevin.luedemann/gpr_gssi.git',
      author='Kevin Luedemann',
      author_email='kevin.luedemann@stud.uni-goettingen.de',
      packages=['gpr_gssi'],
      install_requires=[
        'numpy',
        'bitstruct',
        'numpydoc',
        'matplotlib',
        'scipy',
        'sphinx',
	'h5py',
	'future'
      ],
      zip_safe=False)

